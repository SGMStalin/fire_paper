# The influece of Terrain Rugosity on Wildfires in the tropical Andes

### Context

We hypothesize (i) that the high terrain roughness values function as natural barriers that  prevent the spread of fires. Therefore, areas with high Terrain Roughness Index (TRI; sensu Valencia et al., 2016) should have larger areas covered with forests than areas with low TRI.  A second hypothesis is related to the randomness of fire events. (ii) Fires cannot be a random feature in the landscape. Therefore, the TRI distribution of burned areas (TRIfire) should differ from the TRI distribution of non-burned areas (TRIno fire) and the distribution of all TRI (TRItotal) in a site. This study, makes use of a digital elevation model, satellite imagery, and fire data sets, to analyze the relationships between fires and terrain roughness. Our analysis will allow the classification of the landscape based on its fire vulnerability.


### Methods

#### Study Area

The study area is located along the tropical Andes Mountain range, between 2°77’ and 13°60’ of latitude. The analysis was focused on seven sites, two in Ecuador (Chorreras and Surucucho) and five in Peru (Chochos, Huanmanmarca, Refugio, Pacucha, and Caserococha; see figure 1 and table 1). The sites were selected to test fire dynamics in modern landscapes in sites where palaeoecological reconstructions were performed (Valencia et al., 2016).

![<b>Figure 1.</b> Spatial location of the seven sites](study_area.jpg)
<b>Figure 1.</b> Spatial location of the seven sites

This study, makes use of a digital elevation model (TRI), satellite imagery (NDVI), and fire data sets (GABAM2015), to analyze the relationships between fires and terrain roughness. The complete process was performed in R-project and R Studio (Figure 2).

![<b>Figure 2.</b> Brief explanation of methodology](method.png)
<b>Figure 2.</b> Brief explanation of methodology

### Results and Discussion

![<b>Figure 3.</b> Correlation plots between Mean NDVI, mean TRI and number of fires. Panel (a) shows the correlation between mean TRI and mean NDVI. Panel (b) shows the correlation between mean NDVI and the number of fires. Panel (c) shows the correlation between mean TRI and the number of fires.](results/png/corr_plot.png)
<b>Figure 3.</b> Correlation plots between Mean NDVI, mean TRI and number of fires. Panel (a) shows the correlation between mean TRI and mean NDVI. Panel (b) shows the correlation between mean NDVI and the number of fires. Panel (c) shows the correlation between mean TRI and the number of fires.

We found two positive significant correlations between the three variables analyzed. The highest correlation value was between the mean TRI and NDVI values ($R^{2}$=0.8, P=0.0064; Figure 3a), the second highest significant correlation was between NDVI and the number of fires ($R^{2}$ = 0.31, P = 0.039; Figure 3b). Finally, the lowest but non-significant correlation was between TRI and the number of fires ($R^{2}$ = 0.41, P = 0.119; Figure 3c)

![<b>Figure 4.</b> Brief explanation of the influence of TRI in the fire and vegetation dynamics.](fire_model.png)
<b>Figure 4.</b> Brief explanation of the influence of TRI in the fire and vegetation dynamics.

### Conclusions

Our results show that terrain roughness influence fire dynamics. Flat terrains, recurrent widespread fires that eliminate the vegetation (mainly forests) until the landscape cannot sustain large fires. Forest regeneration is slow over large areas and may be further constrained by grassland fires. Conversely, in rugged terrains, fires were unable to reduce the vegetation over large areas as fires affect discrete compartmentalized patches. Fires in rugged terrains generate a mosaic of forest and burned patches. Forest can regenerate in burned patches from neighboring forest. Currently, rugged zones present a higher frequency of fires due to the high available fuel. However, rugged terrains protect the vegetation since it prevents fire spread and facilitates the regeneration in burned patches. Our analysis demonstrates that terrain roughness plays an important role in fire dynamics, especially at large temporal scales. Thus, rugged terrains could function as zones for the preservation of ecosystems and should be considered in conservation plans.


### References

- <div class="csl-entry">Valencia, B. G., Matthews-Bird, F., Urrego, D. H., Williams, J. J., Gosling, W. D., &#38; Bush, M. (2016). Andean microrefugia: testing the Holocene to predict the Anthropocene. <i>The New Phytologist</i>, <i>212</i>(2), 510–522. https://doi.org/10.1111/nph.14042</div>
