library(raster)
library(terra)
source("scripts/01_watersheds_buff.R")

# Input ndvi images

ndvi_fl <- list.files("data/ndvi", pattern = "*.tif", full.names = T)

# mosaic2mean <- function(...) raster::mosaic(... = ..., fun = mean)
ndvi_l <- list(
  ndvi_caserococha = raster(ndvi_fl[1]),
  ndvi_chochos = raster(ndvi_fl[2]),
  ndvi_chorreras = raster(ndvi_fl[3]),
  ndvi_huanmanmarca = raster(ndvi_fl[4]),
  ndvi_pacucha = raster(ndvi_fl[5]),
  ndvi_refugio = raster(ndvi_fl[6]),
  ndvi_surucucho = raster(ndvi_fl[7])
)

names(ndvi_l) <- catches_names

makeCluster(6) %>% registerDoParallel()
system.time(
  catches_ndvi_l <- foreach(i = catches_names, 
                            .packages = c("raster", "dplyr")) %dopar% {
                              raster::crop(
                                ndvi_l[[i]], catches_buffer[[i]]
                              ) %>% raster::mask(
                                catches_buffer[[i]], 
                                filename = paste0("results/ndvi/", i, ".tif"),
                                overwrite = T
                              )
                            }
)
stopImplicitCluster()

names(catches_ndvi_l) <- catches_names


#### Fix ndvi values [-1, 1]
makeCluster(6) %>% registerDoParallel()
system.time(
  catches_ndvi_l <- foreach(i = catches_names, 
                            .packages = c("raster")) %dopar% {
                              calc(
                                catches_ndvi_l[[i]], fun = function(x){x/10000},
                                filename = paste0("results/ndvi/", i, ".tif"),
                                overwrite = T
                              )
                            }
)
stopImplicitCluster()
