library(tidyverse)
library(sf)
library(raster)
library(terra)
library(magrittr)
library(foreach)
library(doParallel)
library(tmap)

# Input data
dem_ecu <- raster("data/dem/ecuador.tif")

grid_2500m <- read_sf("data/shp/grid_2500m.shp") %>%
  arrange(id) %>%
  mutate(id = fct_reorder(as.character(id), id, min))

grid_order <- grid_2500m$id


### mosaic function to return maps after parallel processing
mosaic2mean <- function(...) raster::mosaic(... = ..., fun = mean)

grids <- NULL
for(i in 1:length(grid_order)){
  grids[[i]] <- grid_2500m[i,]
}
names(grids) <- grid_order

# Make cluster depends on computer capacity
makeCluster(6) %>% registerDoParallel()
system.time(
  dem_mask <- foreach(i = grid_order, .packages = c("raster","sf")) %dopar% {
    crop(dem_ecu, grids[[i]], overwrite = T, 
         filename = paste0("data/dem/ecu_grid/", i, ".tif")) %>%
      mask(grids[[i]], overwrite = T, 
           filename = paste0("data/dem/ecu_grid/mask/", i, ".tif"))
  }
)
stopImplicitCluster()

dem_mask_fl <- list.files("data/dem/ecu_grid/mask/",
                          full.names = T)

dem_mask <- foreach(i = dem_mask_fl) %do% {
  raster(i)
}
names(dem_mask) <- grid_order

## filter values higher than 2500m

makeCluster(6) %>% registerDoParallel()
system.time(
  dem_0na <- foreach(i = grid_order, .packages = c("raster", "terra")) %dopar% {
    raster::clamp(dem_mask[[i]], lower = 2500,
                 useValues = FALSE,
          filename = paste0("data/dem/clamp/", i, ".tif"), overwrite = T)
  }
)
stopImplicitCluster()

names(dem_0na) <- grid_order

makeCluster(6) %>% registerDoParallel()
system.time(
  shp_2500 <- foreach(i = grid_order, .packages = c("raster"), .combine = mosaic2mean) %dopar% {
    raster::terrain(dem_0na[[i]], opt = "TRI", filename = paste0("data/dem/TRI/", i, ".tif"),
         overwrite = T)
  }
)
stopImplicitCluster()

writeRaster(shp_2500, "data/dem/ecu_tri.tif", overwrite = T)
